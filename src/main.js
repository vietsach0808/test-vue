import Vue from 'vue'
import App from './App.vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    name: 'sach'
  },
  getters:{
    name: state => state.name // tuong duong function(state){return state.name}
  },
  mutations:{
    setName(state, name){
      state.name = name;
    }
  },
  actions:{
    updateProfile({commit, getters}){ // cu phap es6 js destructuring, thay vi su dung context thi su dung luon {commit, getters} cua doi tuong context
      commit('setName', 'Nguyen viet sach');
      console.log(getters.name);
    }
  }
})

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  store: store
}).$mount('#app')
